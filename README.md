
<img width="400" src="https://storage.googleapis.com/e_logos/ivo_logo.png"/>

<p>Librería de utilidades para la realización de OVAS llamada IVO.js </p>

## events
>Función que permite generar eventos de una forma corta y rápida, con una opción de generar persistencia en los eventos, se compone de tres parámetros primero tipo de evento, segundo persistencia asociada a una clase o a un id este parámetro es opcional y por último el callback que se produce cuando se efectúa el evento.
~~~~js
ivo("body").on("click",function(e){
    console.log(e.target);
    alert(e.target.className);
});
~~~~
~~~~js
ivo("body").on("click",".miclase",function(e){
    console.log(e.target);
    alert(e.target.className);
});
~~~~
----
## controllers
#### ivo.controllers.get
>Función que permite obtener la página actual y parámetros de la url para ejecutar alguna función, posee tres parámetros, url a buscar, gets que pueda tener la url esta es opcional y por último el callback que se ejecuta cuando el controller concide. 

~~~~js
ivo.controllers.get("index.php","id=1",functi- on(){
  alert("Hola soy controller con parámetros");
});
~~~~
~~~~js
ivo.controllers.get("index.php",function(){
  alert("Hola soy controller sin parámetros");
});
~~~~

#### ivo.odometer
>Función que permite hacer una animación incremental númerica. 

- **start:** Parámetro inicio del contador
- **finish:** parámetro final del contador 
- **step:** Incremento del contador
- **time:** Tiempo de espera entre cada incremento
- **textAux:** Texto auxiliar que acompaña a los valores númericos. 

**Requerimientos:** *ivo.text*

~~~~js
ivo("#contador").odometer({start:200,finish:600,step:10,time:10,textAux:"%"});
~~~~

#### ivo.text
>Función que permite insertar texto en los elementos. 

~~~~js
ivo("#elemento").text("hola mundo!!");
~~~~